<?php

declare(strict_types=1);

namespace Skeleton\App;

use DKX\GoogleReportErrorExceptionWriter\ReportErrorExceptionWriter;
use DKX\SlimLazyHttpExceptions\LazyHttpException;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpException;
use Slim\Handlers\ErrorHandler as SlimErrorHandler;
use Slim\Interfaces\CallableResolverInterface;
use Throwable;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
final class ErrorHandler extends SlimErrorHandler
{
	private ReportErrorExceptionWriter $googleExceptionWriter;

	public function __construct(
		CallableResolverInterface $callableResolver,
		ResponseFactoryInterface $responseFactory,
		ReportErrorExceptionWriter $googleExceptionWriter
	) {
		parent::__construct($callableResolver, $responseFactory);

		$this->googleExceptionWriter = $googleExceptionWriter;
	}

	public function __invoke(ServerRequestInterface $request, Throwable $exception, bool $displayErrorDetails, bool $logErrors, bool $logErrorDetails) : ResponseInterface
	{
		if ($exception instanceof LazyHttpException) {
			$exception = $exception->toRealHttpException($request);
		}

		return parent::__invoke($request, $exception, $displayErrorDetails, $logErrors, $logErrorDetails);
	}

	protected function writeToErrorLog() : void
	{
		if ($this->exception instanceof HttpException) {
			return;
		}

		$this->googleExceptionWriter->writeException($this->exception, $this->request, $this->statusCode);
	}
}
