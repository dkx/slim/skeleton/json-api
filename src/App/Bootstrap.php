<?php

declare(strict_types=1);

namespace Skeleton\App;

use Nette\Configurator;
use function assert;
use function getenv;
use function is_string;

final class Bootstrap
{
	public static function bootWithEnvProjectInfo() : Configurator
	{
		$projectName = getenv('PROJECT_NAME');
		assert(is_string($projectName), 'PROJECT_NAME env must be set');

		$projectVersion = getenv('PROJECT_VERSION');
		assert(is_string($projectVersion), 'PROJECT_VERSION env must be set');

		return static::boot($projectName, $projectVersion)
			->setDebugMode(getenv('PHP_MODE') === 'development');
	}

	public static function boot(string $projectName, string $projectVersion) : Configurator
	{
		$root = __DIR__ . '/../..';

		$configurator = new Configurator();
		$configurator->setTempDirectory('/tmp/app');
		$configurator->setDebugMode(false);
		$configurator->addConfig($root . '/config/config.neon');

		$configurator->addParameters([
			'rootDir' => $root,
			'srcDir' => __DIR__ . '/..',
			'consoleMode' => false,
			'project' => [
				'name' => $projectName,
				'version' => $projectVersion,
			],
		]);

		return $configurator;
	}
}
