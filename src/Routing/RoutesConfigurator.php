<?php

declare(strict_types=1);

namespace Skeleton\Routing;

use DKX\SlimNette\Configuration\ApplicationConfiguratorInterface;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

final class RoutesConfigurator implements ApplicationConfiguratorInterface
{
	public function configure(App $app) : void
	{
		$app->get('/', System\Controllers\HomeController::class);

		$app->group('/v1', function (RouteCollectorProxy $group) : void {
			// todo
		});
	}
}
