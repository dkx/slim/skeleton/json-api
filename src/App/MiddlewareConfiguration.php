<?php

declare(strict_types=1);

namespace Skeleton\App;

use DKX\GoogleTracer\Psr15\TracerControllerSpanDynamicMiddleware;
use DKX\JsonApiMiddleware\JsonApiMiddleware;
use DKX\SlimNette\Configuration\ApplicationConfiguratorInterface;
use Slim\App;
use Slim\Middleware\BodyParsingMiddleware;

final class MiddlewareConfiguration implements ApplicationConfiguratorInterface
{
	private bool $debugMode;

	private ErrorHandler $errorHandler;

	public function __construct(bool $debugMode, ErrorHandler $errorHandler)
	{
		$this->debugMode    = $debugMode;
		$this->errorHandler = $errorHandler;
	}

	public function configure(App $app) : void
	{
		$app->add(TracerControllerSpanDynamicMiddleware::class);
		$app->addRoutingMiddleware();
		$app->add(BodyParsingMiddleware::class);
		$app->add(JsonApiMiddleware::class);

		$errorHandler = $app->addErrorMiddleware($this->debugMode, $this->debugMode, true);
		$errorHandler->setDefaultErrorHandler($this->errorHandler);
	}
}
