<?php

declare(strict_types=1);

namespace Skeleton;

use Contributte\Console\Application;
use DKX\GoogleReportErrorExceptionWriter\ReportErrorExceptionWriter;
use Skeleton\App\Bootstrap;
use Throwable;
use function assert;
use function error_log;

require_once __DIR__ . '/autoload.php';

$container = Bootstrap::bootWithEnvProjectInfo()
	->addParameters(['consoleMode' => true])
	->createContainer();

$application = $container->getByType(Application::class);
assert($application instanceof Application);

try {
	$application->run();
} catch (Throwable $e) {
	try {
		$googleExceptionWriter = $container->getByType(ReportErrorExceptionWriter::class);
		assert($googleExceptionWriter instanceof ReportErrorExceptionWriter);
		$googleExceptionWriter->writeException($e);
	} catch (Throwable $innerE) {
		error_log('Could not write exception to GCP Stackdriver Error Reporting: ' . $innerE->getMessage() . '. Original message was: ' . $e->getMessage());
	}

	throw $e;
}
