<?php

declare(strict_types=1);

namespace Skeleton\Logging;

use DKX\GoogleTracer\Psr15\TracerControllerSpanDynamicMiddleware;
use DKX\GoogleTracer\Trace;
use DKX\MonologCliProcessor\CliProcessor;
use DKX\MonologModifierHandler\ModifierHandler;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use Google\Cloud\Logging\LoggingClient;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\PsrHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\MemoryUsageProcessor;
use Psr\Log\LoggerInterface;
use Skeleton\Http\ServerRequestProviderInterface;
use function assert;
use function urlencode;
use const STDERR;

final class LoggerFactory
{
	private bool $debugMode;

	private bool $consoleMode;

	private string $projectName;

	private ProjectIdProvider $googleProjectIdProvider;

	private ServerRequestProviderInterface $requestProvider;

	private LoggingClient $googleLoggingClient;

	public function __construct(
		bool $debugMode,
		bool $consoleMode,
		string $projectName,
		ProjectIdProvider $googleProjectIdProvider,
		ServerRequestProviderInterface $requestProvider,
		LoggingClient $googleLoggingClient
	) {
		$this->debugMode               = $debugMode;
		$this->consoleMode             = $consoleMode;
		$this->projectName             = $projectName;
		$this->googleProjectIdProvider = $googleProjectIdProvider;
		$this->requestProvider         = $requestProvider;
		$this->googleLoggingClient     = $googleLoggingClient;
	}

	public function create() : LoggerInterface
	{
		$handlers = [
			$this->debugMode ? new StreamHandler(STDERR) : $this->createGoogleLoggingHandler(),
		];

		return new Logger('default', $handlers, [
			new CliProcessor($this->consoleMode),
			new MemoryUsageProcessor(),
		]);
	}

	private function createGoogleLoggingHandler() : HandlerInterface
	{
		$logger = $this->googleLoggingClient->psrLogger(urlencode($this->projectName));

		return new ModifierHandler(new PsrHandler($logger), function (array $record) : array {
			$request = $this->requestProvider->getRequest();
			if ($request !== null) {
				$trace = $request->getAttribute(TracerControllerSpanDynamicMiddleware::REQUEST_SPAN_ATTRIBUTE_NAME);
                assert($trace instanceof Trace || $trace === null);
				if ($trace !== null) {
					$record['context']['stackdriverOptions'] = [
						'trace' => $trace->getLogTracePath($this->googleProjectIdProvider->getProjectId()),
					];
				}
			}

			$record['context']['extra'] = $record['extra'];

			return $record;
		});
	}
}
