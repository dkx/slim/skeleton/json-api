<?php

declare(strict_types=1);

namespace SkeletonTests;

use Mockery;
use Nette\DI\Container;
use PHPUnit\Framework\TestCase as PhpUnitTestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use Skeleton\App\Bootstrap;
use Zend\Diactoros\Response;
use function assert;
use function call_user_func;
use function getmypid;
use function is_callable;

abstract class TestCase extends PhpUnitTestCase
{
	private ?Container $container = null;

	protected function tearDown() : void
	{
		parent::tearDown();
		self::addToAssertionCount(Mockery::getContainer()->mockery_getExpectationCount());
		Mockery::close();
		$this->container = null;
	}

	protected function getContainer() : Container
	{
		if ($this->container === null) {
			$className       = (new ReflectionClass($this))->getShortName();
			$this->container = Bootstrap::boot('app', 'test')
				->setTempDirectory('/tmp/app-tests/' . $className . '_' . $this->getName() . '_' . getmypid())
				->setDebugMode(true)
				->addParameters(['consoleMode' => true])
				->createContainer();
		}

		return $this->container;
	}

	protected function getServiceByType(string $className) : object
	{
		return $this->getContainer()->getByType($className);
	}

	/**
     * @param string[] $args
     */
	protected function callController(string $className, ?ServerRequestInterface $request = null, array $args = []) : ResponseInterface
	{
		$response = new Response();

		if ($request === null) {
			$request = Mockery::mock(ServerRequestInterface::class);
		}

		$controller = $this->getServiceByType($className);
        assert(is_callable($controller));

		$response = call_user_func($controller, $request, $response, $args);
        assert($response instanceof ResponseInterface);
		$response->getBody()->rewind();

		return $response;
	}
}
