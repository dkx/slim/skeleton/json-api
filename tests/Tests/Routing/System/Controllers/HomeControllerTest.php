<?php

declare(strict_types=1);

namespace SkeletonTests\Tests\Routing\System\Controllers;

use Nette\Utils\Json;
use Skeleton\Routing\System\Controllers\HomeController;
use SkeletonTests\TestCase;

final class HomeControllerTest extends TestCase
{
	public function testInvoke() : void
	{
		$response = $this->callController(HomeController::class);
		$data     = Json::decode($response->getBody()->getContents(), Json::FORCE_ARRAY);

		self::assertEquals([], $data);
	}
}
