<?php

declare(strict_types=1);

namespace Skeleton\Http;

use Psr\Http\Message\ServerRequestInterface;

final class ServerRequestProvider implements ServerRequestProviderInterface
{
	private ?ServerRequestInterface $request = null;

	public function reset() : void
	{
		$this->request = null;
	}

	public function getRequest() : ?ServerRequestInterface
	{
		return $this->request;
	}

	public function setRequest(ServerRequestInterface $request) : void
	{
		$this->request = $request;
	}
}
