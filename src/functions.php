<?php

declare(strict_types=1);

namespace Skeleton;

use Spiral\Debug\Dumper;
use Spiral\Debug\Renderer\ConsoleRenderer;

const DEFAULT_DUMPER_MAX_LEVEL = 3;

/**
 * @param mixed $value
 */
function consoleDump($value) : void
{
	/** @var Dumper|null $dumper */
	static $dumper;

	if ($dumper === null) {
		$renderer = new ConsoleRenderer();

		$dumper = new Dumper();
		$dumper->setMaxLevel(DEFAULT_DUMPER_MAX_LEVEL);
		$dumper->setRenderer(Dumper::ERROR_LOG, $renderer);
	}

	$dumper->dump($value, Dumper::ERROR_LOG);
}
