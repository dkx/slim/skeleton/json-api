<?php

declare(strict_types=1);

namespace Skeleton\GoogleCloud;

use DKX\GoogleTracer\Exporter\Exporter;
use DKX\GoogleTracer\Exporter\GoogleTraceExporter;
use DKX\GoogleTracer\Exporter\VoidExporter;
use Google\Cloud\Trace\TraceClient;

final class TracerExporterFactory
{
	private bool $debugMode;

	private TraceClient $client;

	public function __construct(bool $debugMode, TraceClient $client)
	{
		$this->debugMode = $debugMode;
		$this->client    = $client;
	}

	public function create() : Exporter
	{
		return $this->debugMode ?
			new VoidExporter() :
			new GoogleTraceExporter($this->client);
	}
}
