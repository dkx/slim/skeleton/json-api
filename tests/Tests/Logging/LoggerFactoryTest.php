<?php

declare(strict_types=1);

namespace SkeletonTests\Tests\Logging;

use DKX\MonologCliProcessor\CliProcessor;
use DKX\MonologModifierHandler\ModifierHandler;
use DKX\NetteGCloud\ProjectId\ProjectIdProvider;
use Google\Cloud\Logging\LoggingClient;
use Mockery;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\MemoryUsageProcessor;
use Skeleton\Http\ServerRequestProviderInterface;
use Skeleton\Logging\LoggerFactory;
use SkeletonTests\TestCase;
use function assert;

final class LoggerFactoryTest extends TestCase
{
	public function testCreateDebugMode() : void
	{
		$googleProjectIdProvider = Mockery::mock(ProjectIdProvider::class)
			->shouldReceive('getProjectId')->andReturn('app-id')->getMock();
		assert($googleProjectIdProvider instanceof ProjectIdProvider);

		$requestProvider = Mockery::mock(ServerRequestProviderInterface::class);
		$loggingClient   = new LoggingClient(['projectId' => 'app-id']);

		$factory = new LoggerFactory(
			true,
			true,
			'app',
			$googleProjectIdProvider,
			$requestProvider,
			$loggingClient
		);

		$logger = $factory->create();
        assert($logger instanceof Logger);

		self::assertCount(1, $logger->getHandlers());
		self::assertCount(2, $logger->getProcessors());
		self::assertInstanceOf(StreamHandler::class, $logger->getHandlers()[0]);
		self::assertInstanceOf(CliProcessor::class, $logger->getProcessors()[0]);
		self::assertInstanceOf(MemoryUsageProcessor::class, $logger->getProcessors()[1]);
	}

	public function testCreateWithGoogleLogging() : void
	{
		$googleProjectIdProvider = Mockery::mock(ProjectIdProvider::class)
			->shouldReceive('getProjectId')->andReturn('app-id')->getMock();
		assert($googleProjectIdProvider instanceof ProjectIdProvider);

		$requestProvider = Mockery::mock(ServerRequestProviderInterface::class);
		$loggingClient   = new LoggingClient(['projectId' => 'app-id']);

		$factory = new LoggerFactory(
			false,
			true,
			'app',
			$googleProjectIdProvider,
			$requestProvider,
			$loggingClient
		);

		$logger = $factory->create();
        assert($logger instanceof Logger);

		self::assertCount(1, $logger->getHandlers());
		self::assertCount(2, $logger->getProcessors());
		self::assertInstanceOf(ModifierHandler::class, $logger->getHandlers()[0]);
		self::assertInstanceOf(CliProcessor::class, $logger->getProcessors()[0]);
		self::assertInstanceOf(MemoryUsageProcessor::class, $logger->getProcessors()[1]);
	}
}
